package cn.db.crawl.data.processor;

import cn.db.crawl.data.entity.Air7Day;
import cn.db.crawl.data.entity.AirDailyAqi;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 16:25
 */
public class AirAqiProcessor implements PageProcessor {

    private String cityId;

    public AirAqiProcessor(String cityId) {
        this.cityId = cityId;
    }

    @Override
    public void process(Page page) {
        // 数据接口
        // http://www.weather.com.cn/air/

        AirDailyAqi airDailyAqi = new AirDailyAqi();
        String aqi = page.getHtml().xpath("//div[@class='aqi']/em/tidyText()").toString();
        String pm10 = page.getHtml().xpath("//i[@class='pm10']/tidyText()").toString();
        String pm25 = page.getHtml().xpath("//i[@class='pm25']/tidyText()").toString();
        String co = page.getHtml().xpath("//i[@class='co']/tidyText()").toString();
        String no2 = page.getHtml().xpath("//i[@class='no2']/tidyText()").toString();
        String so2 = page.getHtml().xpath("//i[@class='so2']/tidyText()").toString();

        airDailyAqi.setCityId(cityId);
        airDailyAqi.setWDate(LocalDate.now());
        airDailyAqi.setHour(LocalDateTime.now().getHour());
        airDailyAqi.setAqi(NumberUtil.parseDouble(aqi));
        airDailyAqi.setPm10(NumberUtil.parseDouble(pm10.trim()));
        airDailyAqi.setPm25(NumberUtil.parseDouble(pm25));
        airDailyAqi.setCo(NumberUtil.parseDouble(co));
        airDailyAqi.setNo2(NumberUtil.parseDouble(no2));
        airDailyAqi.setSo2(NumberUtil.parseDouble(so2));

        page.putField("airAqi", airDailyAqi);
    }

    @Override
    public Site getSite() {
        return Site.me().setDomain("www.weather.com.cn");
    }

}
