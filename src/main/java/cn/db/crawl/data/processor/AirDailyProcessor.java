package cn.db.crawl.data.processor;

import cn.db.crawl.data.entity.Air7Day;
import cn.hutool.core.util.NumberUtil;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 16:25
 */
public class AirDailyProcessor implements PageProcessor {

    private String cityId;

    public AirDailyProcessor(String cityId) {
        this.cityId = cityId;
    }

    @Override
    public void process(Page page) {
        List<Air7Day> air7DayList = new ArrayList<>();

        for (int i = 1; i <= 7; i++) {
            // 1 day
            // 白天气象文字
            Air7Day air7Day = new Air7Day();
            String dayWord = page.getHtml().xpath("//div[@id='7d']/ul/li["+ i +"]/p[1]/text()").toString();
            String daytimeCode = page.getHtml().xpath("//div[@id='7d']/ul/li["+ i +"]/big[1]/@class").toString();
            String dayNightCode = page.getHtml().xpath("//div[@id='7d']/ul/li["+ i +"]/big[1]/@class").toString();

            int highTemperature = NumberUtil.parseInt(page.getHtml().xpath("//div[@id='7d']/ul/li["+ i +"]/p[2]/span/text()").toString());
            int lowTemperature = NumberUtil.parseInt(page.getHtml().xpath("//div[@id='7d']/ul/li["+ i +"]/p[2]/i/text()").toString().replace("℃", ""));

            String windDirection = page.getHtml().xpath("//div[@id='7d']/ul/li["+ i +"]/p[3]/em/span[1]/@title").toString();
            String windScale = page.getHtml().xpath("//div[@id='7d']/ul/li["+ i +"]/p[3]/i/text()").toString();

            if (dayWord.contains("转")) {
                String[] dayWordArray = dayWord.split("转");
                String daytime = dayWordArray[0];
                String dayNight = dayWordArray[1];

                air7Day.setTextForDay(daytime);
                air7Day.setTextForNight(dayNight);
            } else {
                air7Day.setTextForDay(dayWord);
                air7Day.setTextForNight(dayWord);
            }

            air7Day.setCodeForDay(daytimeCode.substring(daytimeCode.length() - 2, daytimeCode.length()));
            air7Day.setCodeForNight(dayNightCode.substring(dayNightCode.length() - 2, dayNightCode.length()));

            air7Day.setHighTemperature(highTemperature);
            air7Day.setLowTemperature(lowTemperature);

            air7Day.setWindDirection(windDirection);
            air7Day.setWindScale(windScale);

            air7Day.setCityId(cityId);
            air7Day.setWDate(LocalDate.now().plusDays(i - 1));

            air7DayList.add(air7Day);

        }

        page.putField("air7DayList", air7DayList);
    }

    @Override
    public Site getSite() {
        return Site.me().setDomain("www.weather.com.cn");
    }

}
