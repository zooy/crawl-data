package cn.db.crawl.data.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author：zooooooooy
 * @date: 2022/12/3 - 0:35
 */
@Data
@TableName("t_bing_image")
public class BingImage {

    @TableId
    private int id;

    private LocalDate imageDate;

    private String imageUrl;

}
