package cn.db.crawl.data.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 17:23
 * AQI - 空气质量
 */
@Data
@TableName("t_air_aqi")
public class AirDailyAqi {

    @TableId
    private int id;

    private String cityId;

    private int hour;

    private LocalDate wDate;

    /**
     * 1. 0~50 优
     * 2. 50~100 良
     * 3. 100~150 轻度污染
     * 4. 150~200 中度污染
     * 5. 200~300 重度污染
     * 6. 300~500 严重污染
     */
    private double aqi;

    private double pm25;

    private double pm10;

    private double so2;

    private double co;

    private double no2;

    private double o3;

}
