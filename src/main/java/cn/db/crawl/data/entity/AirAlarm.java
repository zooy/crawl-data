package cn.db.crawl.data.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 16:43
 * @desc 天气预警
 */
@Data
@TableName("t_air_alarm")
public class AirAlarm {

    private int cityId;

    private int hour;

    private LocalDate wDate;

    private int airIndex;

    private String airLevel;

    private String airType;


}
