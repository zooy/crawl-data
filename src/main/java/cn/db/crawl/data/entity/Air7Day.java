package cn.db.crawl.data.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 16:37
 */
@TableName("t_air_7day")
@Data
public class Air7Day {

    @TableId
    private int id;

    private String cityId;

    private LocalDate wDate;

    private String textForDay;

    private String codeForDay;

    private String textForNight;

    private String codeForNight;

    private int highTemperature;

    private int lowTemperature;

    private double chanceOfRain;

    private String windDirection;

    private String windScale;

}
