package cn.db.crawl.data;

import cn.hutool.extra.spring.EnableSpringUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableSpringUtil
public class CrawlDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrawlDataApplication.class, args);
	}

}
