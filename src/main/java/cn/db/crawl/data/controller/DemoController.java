package cn.db.crawl.data.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 16:12
 */
@RestController
@RequestMapping("v1/crawl")
public class DemoController {

    @PostMapping("demo")
    public ResponseEntity<String> demo() {

        return new ResponseEntity<>("测试", HttpStatus.OK);

    }

}
