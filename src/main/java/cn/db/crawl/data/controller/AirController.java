package cn.db.crawl.data.controller;

import cn.db.crawl.data.service.Air7DayService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author：zooooooooy
 * @date: 2023/10/26 - 16:58
 */
@RestController
@RequestMapping("v1/air")
@AllArgsConstructor
public class AirController {

    private Air7DayService air7DayService;

    @PostMapping("last7DayAir")
    public ResponseEntity last7DayAir() {
        // 近7天的天气
        return new ResponseEntity<>(air7DayService.last7DayAir(), HttpStatus.OK);
    }

}
