package cn.db.crawl.data.pipeline;

import cn.db.crawl.data.entity.Air7Day;
import cn.db.crawl.data.entity.AirDailyAqi;
import cn.db.crawl.data.mapper.Air7DayMapper;
import cn.db.crawl.data.mapper.AirAqiMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.List;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 19:05
 */
@Component
@AllArgsConstructor
public class AirAqiPipeline implements Pipeline {

    private AirAqiMapper airAqiMapper;

    @Override
    public void process(ResultItems resultItems, Task task) {

        if (!resultItems.isSkip()) {
            AirDailyAqi airDailyAqi = resultItems.get("airAqi");
            // 判断是否存在
            AirDailyAqi localAirDailyAqi = airAqiMapper.selectOne(new LambdaQueryWrapper<AirDailyAqi>()
                    .eq(AirDailyAqi::getWDate, airDailyAqi.getWDate())
                    .eq(AirDailyAqi::getHour, airDailyAqi.getHour())
            );
            if (localAirDailyAqi != null) {
                // 存在更新
                airDailyAqi.setId(localAirDailyAqi.getId());
                airAqiMapper.updateById(airDailyAqi);
            } else {
                // 不存在则插入
                airAqiMapper.insert(airDailyAqi);
            }
        }

    }
}
