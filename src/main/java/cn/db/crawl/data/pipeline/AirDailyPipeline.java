package cn.db.crawl.data.pipeline;

import cn.db.crawl.data.entity.Air7Day;
import cn.db.crawl.data.mapper.Air7DayMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.List;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 19:05
 */
@Component
@AllArgsConstructor
public class AirDailyPipeline implements Pipeline {

    private Air7DayMapper air7DayMapper;

    @Override
    public void process(ResultItems resultItems, Task task) {

        if (!resultItems.isSkip()) {
            List<Air7Day> air7DayList = resultItems.get("air7DayList");
            air7DayList.forEach(air7Day -> {
                // 判断是否存在
                Air7Day localAir7Day = air7DayMapper.selectOne(new LambdaQueryWrapper<Air7Day>().eq(Air7Day::getWDate, air7Day.getWDate()));
                if (localAir7Day != null) {
                    // 存在更新
                    air7Day.setId(localAir7Day.getId());
                    air7DayMapper.updateById(air7Day);
                } else {
                    // 不存在则插入
                    air7DayMapper.insert(air7Day);
                }
            });
        }

    }
}
