package cn.db.crawl.data.service;

import cn.db.crawl.data.entity.Air7Day;
import cn.db.crawl.data.mapper.Air7DayMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author：zooooooooy
 * @date: 2023/10/26 - 17:16
 */
@AllArgsConstructor
@Service
public class Air7DayService {

    private Air7DayMapper air7DayMapper;

    public List<Air7Day> last7DayAir() {

        return air7DayMapper.last7DayAir();
    }

}
