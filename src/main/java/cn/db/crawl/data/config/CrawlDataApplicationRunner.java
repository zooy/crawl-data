package cn.db.crawl.data.config;

import cn.db.crawl.data.download.ChromeHeadlessDownloader;
import cn.db.crawl.data.pipeline.AirAqiPipeline;
import cn.db.crawl.data.pipeline.AirDailyPipeline;
import cn.db.crawl.data.pipeline.BingImagePipeline;
import cn.db.crawl.data.processor.AirAqiProcessor;
import cn.db.crawl.data.processor.AirDailyProcessor;
import cn.db.crawl.data.processor.BingImageProcessor;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;

/**
 * @author：zooooooooy
 * @date: 2022/12/1 - 17:49
 */
@Component
@RequiredArgsConstructor
public class CrawlDataApplicationRunner implements ApplicationRunner{

    @Override
    public void run(ApplicationArguments args) throws Exception {

    }

}
