package cn.db.crawl.data.mapper;

import cn.db.crawl.data.entity.Air7Day;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @author：zooooooooy
 * @date: 2022/12/2 - 10:01
 */
public interface Air7DayMapper extends BaseMapper<Air7Day> {

    List<Air7Day> last7DayAir();

}
