package cn.db.crawl.data.mapper;

import cn.db.crawl.data.entity.BingImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author：zooooooooy
 * @date: 2022/12/3 - 0:37
 */
public interface BingImageMapper extends BaseMapper<BingImage> {
}
