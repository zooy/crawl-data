package cn.db.crawl.data.mapper;

import cn.db.crawl.data.entity.AirDailyAqi;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author：zooooooooy
 * @date: 2022/12/2 - 10:01
 */
public interface AirAqiMapper extends BaseMapper<AirDailyAqi> {



}
